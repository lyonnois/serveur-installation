### Description
Script pour automatiser l'installation de serveurs, machines virtuelles ou serveurs physiques.
Propose d'installer un serveur LAMP.

##### Applications installées de base
* éditeur de texte (emacs, nano ou vim)
* rkhunter
* fail2ban
* portsentry
* clamav
* proftpd

##### Serveur LAMP
* apache2
* php5
* mysql-server-5.5

##### Changements effectués
* configurer langue et clavier
* modification mot de passe root
* création d'un utilisateur principal et de nouveaux utilisateurs 
* création d'un journal récapitulatif d'installation

### Pré-requis
Importer les scripts :
* firewall
* upgrade
* antivirus