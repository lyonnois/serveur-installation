#!/bin/bash


## Script pour automatiser l'installation de serveurs, machines virtuelles ou serveurs physiques 


ajout_ip()
{
	cp /etc/portsentry/portsentry.ignore.static /etc/portsentry/portsentry.bis
	sed "26a\ $adresse" /etc/portsentry/portsentry.bis > /etc/portsentry/portsentry.ignore.static
	rm /etc/portsentry/portsentry.bis

	echo "ignoreip = $adresse" >> /etc/fail2ban/jail.local
}

script()
{
	chmod +x /home/antivirus
	chmod +x /home/upgrade
	chmod +x /home/firewall	
	mv /home/antivirus /etc/cron.weekly/antivirus
	mv /home/upgrade /etc/cron.daily/upgrade
	mv /home/firewall /home/scripts/firewall
}

install_base()
{
	apt-get install rkhunter
	apt-get install fail2ban portsentry
	apt-get install clamav
	apt-get install proftpd
}

install_lamp()
{
	apt-get install apache2
	apt-get install php5
	apt-get install mysql-server-5.5
}

firewall_a()
{
	sed '44,51d' /home/scripts/firewall > /home/scripts/firewall_temp
	mv /home/scripts/firewall_temp /home/scripts/firewall
	chmod +x /home/scripts/firewall
	/home/scripts/firewall
	cp /home/scripts/firewall /etc/init.d/
}

firewall_b()
{
	/home/scripts/firewall
	cp /home/scripts/firewall /etc/init.d/
}


# Vérification de l'installation des scripts

read -p "Avez-vous également importer les scripts (firewall + upgrade + antivirus) dans ce dossier? [O,n]" choix5

if [ $choix5 = "O" ] 	
then
	script

else	
	echo -e "\nVeuillez procéder à l'importation des scripts dans ce dossier et relancer le script."
	exit
fi


mkdir -p /home/scripts/log/


# Changer langue et clavier

dpkg-reconfigure locales
#raspi-config


# Modification mot de passe root et création utilisateur administrateur

echo -e "\n\n========== Installation du serveur ========== \n\n\n Important :\n  Désirez-vous changer le mot de passe pour root maintenant ? [O,n]"

read choix1

if [ $choix1 = "O" ] 
then
	passwd root
fi

read -p "  Créer un utilisateur administrateur ? [O,n]" choix2 

if [ $choix2 = "O" ] 
then
	echo -e "\n\n  Saisir le login de l'utilisateur désiré, sa désignation complète, son mot de passe \n  Sous la forme : admin \"Administrateur numéro 1\" mot_de_passe"
	read loga1 loga2 loga3

	useradd -m -c "$loga2" $loga1 -s /bin/bash

	echo "$loga1:$loga3" | chpasswd
fi


# Installation du serveur selon son type

echo -e "\n\n  Installation sur \n -- 'A' machine virtuelle \n -- 'B' machine virtuelle avec serveur LAMP [Apache2 - Php5 - Mysql]\n -- 'C' serveur physique \n -- 'D' serveur physique avec serveur LAMP [Apache2 - Php5 - Mysql]\n\n Saisir la lettre correspondante à votre choix ou q pour quitter"

read choix3

apt-get update
apt-get upgrade


case $choix3 in

	q)
                
		;;

	A)
		install_base 
		firewall_a
		lamp=0
		;;
		
	B)
		install_base
		install_lamp
		firewall_b
		lamp=1
		;;

	C)
		install_base
		firewall_a
		lamp=0
		;;

	D)
		install_base
		install_lamp
		firewall_b
		lamp=1
		;;

	*)	
		echo "Choix invalide"
		;;

esac


#Choix de l'éditeur de texte

echo -e "  Quel éditeur désirez-vous installer ?\n- [N]ano\n- [V]im\n- [E]macs\nSaisir la lettre correspondant à votre choix [p pour passer]\n"
read choix6

case $choix6 in
	
	N)
		apt-get install nano
		;;
	V)
		apt-get install vim
		;;
	E)
		apt-get install emacs
		;;
	p)	
		;;
	*)		
		echo "Choix invalide"
		;;

esac

#Invitation à créer de nouveaux utilisateurs

read -p "  Créer un autre utilisateur ? [O,n]" choix2 

while [ $choix2 = "O" ] 
do
	read -p "  Saisir le login de l'utilisateur désiré, sa désignation complète, son mot de passe, sous la forme : utilisateur \"Nom prénom\" mot_de_passe  " logb1 logb2 logb3

	useradd -m -c "$logb2" $logb1 -s /bin/bash

	echo "$logb1:$logb3" | chpasswd 

	read -p "Créer de nouveau un utilisateur ? [O,n]" choix2
done


#Ajout d'une adresse sûre dans les configurations fail2ban et portsentry

read -p "  Veuillez saisir l'adresse du poste distant pour administration ssh [p pour passer]  " adresse

if [ $adresse = "q" ] 
then

	echo "\n  Aucune adresse distante n'a été ajouté pour fail2ban et portsentry"
else 
	ajout_ip $adresse

fi


#Création du journal d'installation

touch /home/scripts/log/recapitulatif.log

echo -e "***** Récapitulatif de l'installation *****\n\n*** Utilisateur : $loga2 avec login $loga1\n\n*** Liste des paquets installés :\n - sécurité : fail2ban, portsentry, clamav, rkhunter\n\n*** Liste des scripts :\n - firewall, antivirus(clamav + rkhunter), upgrade\n\n" > /home/scripts/log/recapitulatif.log

if [ $lamp = "1" ]
then
	sed "6a \ _ serveur LAMP : apache2, php5, mysql-server-5.6" /home/scripts/log/recapitulatif.log > /home/scripts/log/recapitulatif-temp.log
	mv /home/scripts/log/recapitulatif-temp.log /home/scripts/log/recapitulatif.log
fi

if [ $choix2 = "O" ]
then
	sed "3a\ $1" /home/scripts/log/recapitulatif.log > /home/scripts/log/recapitulatif-temp.log
	mv /home/scripts/log/recapitulatif-temp.log /home/scripts/log/recapitulatif.log
fi

if [ $adresse != "q" ] 
then
	echo -e "*** Adresse accès distant ajoutée : $adresse\n" >> /home/scripts/log/recapitulatif.log
fi

echo -e "*** Ports ouverts : 22, 53, 123, 80, 443\n" >> /home/scripts/log/recapitulatif.log


exit
